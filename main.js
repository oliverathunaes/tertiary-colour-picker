const {app, BrowserWindow, Menu, MenuItem} = require('electron');
const url = require('url');
const path = require('path');

let win;

function create_window() {
    win = new BrowserWindow();

    win.loadURL(url.format({
	pathname: path.join(__dirname, 'index.html'),
	protocol: 'file:',
	slashes:true
    }));

    win.on('closed', function() {
	app.quit()
    });

    const mainMenu = Menu.buildFromTemplate(main_menu_template);
    Menu.setApplicationMenu(mainMenu);
}

function quit_program() {
    app.quit()
}

const main_menu_template = [
    {
	label:'File',
	submenu: [
	    {
		label: 'Quit',
		accelerator: process.platform == 'darwin' ? 'Command+Q' : 'Ctrl+Q',
		click: quit_program
	    }
	]
    },
    {
	label:'View',
	submenu: [
	    {role: 'toggledevtools'}
	]
    }
];

app.on('ready', create_window);

// Quit when all windows are closed
app.on('window-all-closed', function() {
    if(!process.platform == 'darwin') {
	app.quit();
    }
});


function update(jscolor) {
    // 'jscolor' instance can be used as a string
    document.getElementById('rect').style.backgroundColor = '#' + jscolor
}
